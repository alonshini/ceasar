﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

public class PieceCcontrolView : MonoBehaviour
{
    [SerializeField] GameObject m_confirmButton;
    [SerializeField] GameObject m_mirrorButton;
    [SerializeField] RectTransform m_rect;

    [SerializeField] private StaticPieceView m_rotateCWDisplay;
    [SerializeField] private StaticPieceView m_rotateCCWDisplay;
    [SerializeField] private StaticPieceView m_mirrorDisplay;
    [SerializeField] GameObject m_rotateCWButton;
    [SerializeField] GameObject m_rotateCCWButton;
    private bool m_miniDisplaysCreated = false;
    private PieceView m_currPieceView;

    public void Init(Piece piece, PieceView pieceView,bool showConfirm)
    {
        m_currPieceView = pieceView;
        m_mirrorButton.SetActive(piece.MirrorEnabled);
        m_rotateCWDisplay.gameObject.SetActive(piece.RotationEnabled);
        m_rotateCCWDisplay.gameObject.SetActive(piece.RotationEnabled);
        m_rotateCWButton.gameObject.SetActive(piece.RotationEnabled);
        m_rotateCCWButton.gameObject.SetActive(piece.RotationEnabled);

        
        if (m_miniDisplaysCreated)
        {
            UpdateAllMiniDisplays(piece);
        }
        else
        {
            Piece CWPiece = GetPieceCopy(piece);
            Vector2[] newOffsetsCW = ManagerView.Instace.GetRotatedVectors(CWPiece.Offsets, true, false, CWPiece);
            m_rotateCWDisplay.Init(m_rotateCWDisplay.transform,0.2f,newOffsetsCW,Color.white,calcPixelOffset(newOffsetsCW));
            
            Piece CCWPiece = GetPieceCopy(piece);
            Vector2[] newOffsetsCCW = ManagerView.Instace.GetRotatedVectors(CCWPiece.Offsets, false, false, CCWPiece);
            m_rotateCCWDisplay.Init(m_rotateCCWDisplay.transform,0.2f,newOffsetsCCW,Color.white,calcPixelOffset(newOffsetsCCW));
            
            
            Piece MirrorPiece = GetPieceCopy(piece);
            Vector2[] newOffsetsMirror = ManagerView.Instace.GetRotatedVectors(MirrorPiece.Offsets, false, true, MirrorPiece);
            m_mirrorDisplay.Init(m_mirrorDisplay.transform,0.2f,newOffsetsMirror,Color.white,calcPixelOffset(newOffsetsMirror));
            
            m_miniDisplaysCreated = true;
        }
            
        Move(pieceView.GetControllerPosition());
        ShowHideUI(true,showConfirm);
    }

    private Vector2 calcPixelOffset(Vector2[] Offsets)
    {
        int minx=0;
        int maxx = 0;
        int miny=0;
        int maxy = 0;
        
        for (int i = 0; i < Offsets.Length; i++)
        {
            int posx = Convert.ToInt32(Offsets[i].x);
            int posy = Convert.ToInt32(Offsets[i].y);

            maxx = Mathf.Max(posx, maxx);
            minx = Mathf.Min(posx, minx);
            
            maxy = Mathf.Max(posy, maxy);
            miny = Mathf.Min(posy, miny);
        }

        Vector2 temp = new Vector2(0-(maxx + minx) / 2f, 0-(maxy + miny) / 2f);
        
        Vector2 prefabCenter = new Vector2(0.5f,0.5f) - temp;
        
       return  (new Vector2(0.5f,0.5f)- prefabCenter) * 140 * 0.25f;
    }
    
    
    private void UpdateAllMiniDisplays(Piece piece)
    {
       
        Piece CWPiece = GetPieceCopy(piece);
        Vector2[] newOffsetsCW = ManagerView.Instace.GetRotatedVectors(CWPiece.Offsets, true, false, CWPiece);
        Vector2 offset = calcPixelOffset(newOffsetsCW);
        UpdateMiniDisplay(m_rotateCWDisplay,newOffsetsCW,offset);
        
        Piece CCWPiece = GetPieceCopy(piece);
        Vector2[] newOffsetsCCW = ManagerView.Instace.GetRotatedVectors(CCWPiece.Offsets, false, false, CCWPiece);
        offset = calcPixelOffset(newOffsetsCCW);
        UpdateMiniDisplay(m_rotateCCWDisplay,newOffsetsCCW,offset);
            
            
        Piece MirrorPiece = GetPieceCopy(piece);
        Vector2[] newOffsetsMirror = ManagerView.Instace.GetRotatedVectors(MirrorPiece.Offsets, false, true, MirrorPiece);
        offset = calcPixelOffset(newOffsetsMirror);
       UpdateMiniDisplay(m_mirrorDisplay,newOffsetsMirror,offset);

    }
    
    private Piece GetPieceCopy(Piece piece)
    {
        return new Piece(piece);
    }
    
    public void Move(Vector2 newPos)
    {
        m_rect.localPosition = newPos;
    }

    public void TrashClicked()
    {
        ManagerView.Instace.TrashClicked();
    }
    
    public void ShowHideUI(bool show,bool showConfirm)
    {
        gameObject.SetActive(show);
        m_confirmButton.SetActive(showConfirm);
    }

    //used to update displays after rotate or mirror
    public void UpdateState(Piece piece)
    {
        UpdateAllMiniDisplays(piece);
    }
    public void ShowHideConfirm(bool showConfirm)
    {
        m_confirmButton.SetActive(showConfirm);
    }
    
    public void RotateClicked(bool CW)
    {
        ManagerView.Instace.RotateClicked(CW);
    }

    public void MirrorClicked()
    {
        ManagerView.Instace.MirrorClicked();
    }

    public void ConfirmClicked()
    {
        ManagerView.Instace.ConfirmClicked();
    }

    private void UpdateMiniDisplay(StaticPieceView staticPieceView,Vector2[] positions,Vector2 offset)
    {
        staticPieceView.GetComponent<RectTransform>().localPosition = offset;
        staticPieceView.UpdateParams(positions);
    }

    public void Reset()
    {
        ShowHideUI(false,false);
        m_currPieceView = null;
    }
}
