﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Solution
{
    private List<Piece> m_levelSolution = new List<Piece>();

    public Solution(List<Piece> mLevelSolution)
    {
        m_levelSolution = mLevelSolution;
    }


    
    public List<Piece> LevelSolution => m_levelSolution;
}
