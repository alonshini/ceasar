﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public static class Utils 
{
    // Start is called before the first frame update
    public static int OutOfRange(bool checkX, Vector2 vector2)
    {
        if(checkX)
        {
            int tempX = Convert.ToInt32(vector2.x);

            if (tempX < 1)
                return 1 - tempX;
            if (tempX > ManagerView.WIDTH)
                return ManagerView.WIDTH - tempX;

            return 0;
        }
        else
        {
            int tempY = Convert.ToInt32(vector2.y);

            if (tempY < 1)
                return 1 - tempY;
            if (tempY > ManagerView.HEIGHT)
                return ManagerView.HEIGHT - tempY;

            return 0;
        }
    }
    
    public static bool IsInRange(Vector2 vector2)
    {

        int tempX = Convert.ToInt32(vector2.x);
        int tempY = Convert.ToInt32(vector2.y);

        if(tempX>=1 && tempX<= ManagerView.WIDTH)
            if(tempY>=1 && tempY<= ManagerView.HEIGHT)
                return true;

        return false;
    }
    
   

    public static void Shuffle<T>(this IList<T> list)  
    {  
    
        int n = list.Count;  
        while (n > 1) {  
            n--;  
            int k = UnityEngine.Random.Range(0,n + 1);  
            T value = list[k];  
            list[k] = list[n];  
            list[n] = value;  
        }  
    }

    public static Color GenerateDarkenColorVersion(Color color, float darkenAmount )
    {
        float H, S, V;

        Color.RGBToHSV(color, out H, out S, out V);

        return Color.HSVToRGB(H, S, V * darkenAmount);
    }
    
    public static float remap(float val, float in1, float in2, float out1, float out2)
    {
        return out1 + (val - in1) * (out2 - out1) / (in2 - in1);
    }
    
    public static string ReadDataFile()
    {
        TextAsset level = Resources.Load<TextAsset>("test");
        //Read the text from directly from the test.txt file
        StreamReader reader = new StreamReader(new MemoryStream(level.bytes)); 
        string result = reader.ReadToEnd();
        reader.Close();
        return result;
    }
    
    public static void WriteString(string data)
    {
        string path = "Assets/Resources/test.txt";
        //Write some text to the test.txt file
        StreamWriter writer = new StreamWriter(path, true);
        writer.WriteLine("Test");
        writer.Close();
       
    }
}
