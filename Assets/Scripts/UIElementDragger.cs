﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIElementDragger : MonoBehaviour,IPointerDownHandler
{

    private bool dragging;
    private RectTransform m_rect;
    float factor;
    Vector2 offset = new Vector2();
    Vector2 parentOffset = new Vector2();

    private void Awake()
    {
        factor = 1 / 0.441f;
        offset = new Vector2(Screen.width, Screen.height);
        parentOffset = new Vector2(32, 64);

    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("Point: " + Input.mousePosition * ManagerView.Instace.ScreenToPixels);
        Vector2 currPos = ManagerView.Instace.PointToSquere(Input.mousePosition * ManagerView.Instace.ScreenToPixels);
        //Debug.Log("World:" +Camera.main.ScreenToWorldPoint(Input.mousePosition));
        //Debug.Log("Viewport:" +Camera.main.ScreenToViewportPoint(Input.mousePosition));
        Debug.Log("SquerePos:" + currPos);
    }
}