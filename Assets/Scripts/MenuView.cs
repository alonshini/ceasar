﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MenuView : MonoBehaviour
{
    [SerializeField] private Slider m_slider;
    [SerializeField] private Slider m_solutionSlider;
    [SerializeField] private TMP_Text m_sliderValue;
    [SerializeField] private TMP_Text m_solutionSliderValue;
    public void MenuButtonClicked()
    {
        ManagerView.Instace.MenuClicked((int) m_slider.value);
    }

    public void SliderValueChanged()
    {
        m_sliderValue.text = ((int) m_slider.value).ToString();
       
    }

    public void SolutionSliderValueChanged()
    {
        m_solutionSliderValue.text = ((int) m_solutionSlider.value).ToString();
       
    }
    
    public void FullClicked()
    {
        //Load a full solution
        ManagerView.Instace.MenuClicked(1000,(int)m_solutionSlider.value);
    }

    public void EmptyClicked()
    {
        ManagerView.Instace.MenuClicked(0);
    }

    public void FillSOlutionSlider(int max)
    {
        m_solutionSlider.maxValue = max;
    }
}
