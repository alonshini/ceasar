﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class PieceView : MonoBehaviour
{
    #region varibles
    
    public const float MOVE_TIME=0.25f;
    private const float PUT_DOWN_TIME = 0.1f;
    private const float BOARD_SIZE = 0.98f;
    private const float GAME_SIZE = 1f;
    
    int m_posX = 0;
    int m_posY = 0;

    int m_startDragOffsetXUnits = 0;
    int m_startDragOffsetYUnits = 0;

    private Color m_pieceColor;
    
    bool m_drag = false;
    private bool m_inInventory;
    private Vector2 m_inventroyPosition;
    private RectTransform m_rect;
    private TMP_Text m_display;
    private Piece m_pieceModel;
    private Vector2 m_prefabCenter = new Vector2();
    private Vector2 m_prefabCenterPixelOffset = new Vector2();
    
    List<SquerePieceView> m_squeres = new List<SquerePieceView>();
    private Action m_moveDone;
    private bool m_piecePlaced;
    private bool m_pieceSetForever = false;
    public List<SquerePieceView> MSqueres => m_squeres;

    public int MPosX => m_posX;

    public int MPosY => m_posY;

    public Color MPieceColor => m_pieceColor;

    public Piece MPieceModel => m_pieceModel;

    public bool m_is3D;
    
    #endregion

    
    private void Awake()
    {
        if(m_is3D)
            m_rect = GetComponent<RectTransform>();
        
        m_display = GetComponentInChildren<TMP_Text>();
    }
    private void Update()
    {
        if(m_drag)
        {
            //if the mouse moves to a new position from my curr position - check if it can move there - and if so - move it

            Vector2 currPos = ManagerView.Instace.PointToSquere(Input.mousePosition * ManagerView.Instace.ScreenToPixels);

            if ((int)currPos.x != MPosX+m_startDragOffsetXUnits || (int)currPos.y != MPosY+m_startDragOffsetYUnits)
                MovePiece((int)currPos.x, (int)currPos.y);

        }
    }
    public void Init(Piece pieceModel,float scale,bool inInventroy, int posX, int posY,Vector2[] squeres,Color color,Vector2 inventoryPostion)
    {
        m_pieceModel = pieceModel;
        m_inInventory = inInventroy;
        m_pieceColor = color;
    
        m_inventroyPosition = inventoryPostion;
        m_posX = posX;
        m_posY = posY;
        m_pieceSetForever = !pieceModel.PositionEnabled;

        if (m_is3D)
        {
            transform.localScale = new Vector3(scale, scale, scale);
            //transform.sizeDelta = new Vector2(ManagerView.PIECE_WIDTH, ManagerView.PIECE_WIDTH);
        }
        else
        {
            
            m_rect.localScale = new Vector3(scale, scale, scale);
            m_rect.sizeDelta = new Vector2(ManagerView.PIECE_WIDTH, ManagerView.PIECE_WIDTH);
        }

        Vector2[] result = ManagerView.Instace.GetRotatedVectors(m_pieceModel.Offsets, m_pieceModel.CurrRotation, m_pieceModel.CurrMirror);
        m_pieceModel.Offsets = result;
        m_pieceModel.CurrRotation = 0;
        
        foreach (Vector2 item in result)
        {
            GameObject part = Instantiate(ManagerView.Instace.SingleSquerePrefab, transform);

            SquerePieceView squerePieceView = part.GetComponent<SquerePieceView>();

            MSqueres.Add(squerePieceView);

            part.GetComponent<SquerePieceView>().Init(item,DragStarter, DragEnded, color,this,false);

        }

        CalculateCenter();
        
        if (inInventroy)
        {
            MoveToInventory(null,true);
        }
        else
        {
            m_piecePlaced = true;
            
            foreach (SquerePieceView squere in m_squeres)
                squere.SetPieceToBoard(m_pieceSetForever,m_piecePlaced);
            
            
            MoveToBoardPosition(null,m_posX,m_posY,0);
            ManagerView.Instace.MarkBoardWithPiece(m_posX, m_posY, true, MSqueres);
        }
    }

    
    
    private void CalculateCenter()
    {
        int minx=0;
        int maxx = 0;
        int miny=0;
        int maxy = 0;
        
        for (int i = 0; i < m_squeres.Count; i++)
        {
            int posx = m_squeres[i].GetPosX();
            int posy = m_squeres[i].GetPosY();

            maxx = Mathf.Max(posx, maxx);
            minx = Mathf.Min(posx, minx);
            
            maxy = Mathf.Max(posy, maxy);
            miny = Mathf.Min(posy, miny);
        }

        Vector2 temp = new Vector2(0-(maxx + minx) / 2f, 0-(maxy + miny) / 2f);
        
        m_prefabCenter = new Vector2(0.5f,0.5f) - temp;

        if (m_is3D)
        {
            m_prefabCenterPixelOffset =  (new Vector2(0.5f,0.5f)- m_prefabCenter) * 100 * ManagerView.INVENTORY_SCALE;

        }
        else
        {
            m_prefabCenterPixelOffset =  (new Vector2(0.5f,0.5f)- m_prefabCenter) * m_rect.rect.width * ManagerView.INVENTORY_SCALE;
            
        }

    }
    public void PiecePlacedRemovedFromBoard(bool placed)
    {
        foreach (SquerePieceView squere in m_squeres)
            squere.SetPieceToBoard(m_pieceSetForever,placed);
        
        if(placed)
            m_piecePlaced = true;
        
        if(m_is3D)
            MoveAndScaleTween(transform.localPosition,GAME_SIZE,PUT_DOWN_TIME,null);
        else
            MoveAndScaleTween(m_rect.localPosition,GAME_SIZE,PUT_DOWN_TIME,null);
    }
    
    public void RotatePiece(Vector2[] squeres, bool forceRotate=false)
    {
        //check if we can rotate - and keep in range - if we are not in range - need to move the rotated piece

        Vector2 tempPos = new Vector2(MPosX - m_startDragOffsetXUnits, MPosY - m_startDragOffsetYUnits);

        bool canRotoate = true;

        for (int i = 0; i < squeres.Length; i++)
        {
            if (Utils.IsInRange(squeres[i]+ tempPos) == false)
                canRotoate = false;
        }

        if (canRotoate == false && !forceRotate)
        {
            //means that if we rotate on the currCenter - piece will be out of range - need to find the out range - on X than on Y - move the center and rotate

            int maxXOut = 0;

            for (int i = 0; i < squeres.Length; i++)
            {
                int result = Utils.OutOfRange(true, squeres[i] + tempPos);

                if(result>0)
                {
                    if (result > maxXOut && result > Mathf.Abs(maxXOut))
                        maxXOut = result;
                }
                else if(result < 0)
                {
                    if (Mathf.Abs(result) > Mathf.Abs(maxXOut))
                        maxXOut = result;
                }
                
            }

            //we are out of range on the X - need to move on the X to complete rotate
            if(maxXOut!=0)
            {
                MovePiece(MPosX - m_startDragOffsetXUnits + maxXOut, MPosY - m_startDragOffsetYUnits);
            }

            //updating tempPos - because maybe i moved the piece on the X axis in prev search

            tempPos = new Vector2(MPosX - m_startDragOffsetXUnits, MPosY - m_startDragOffsetYUnits);
            int maxYOut = 0;

            for (int i = 0; i < squeres.Length; i++)
            {
                int result =Utils.OutOfRange(false, squeres[i] + tempPos);

                if (result > 0)
                {
                    if (result > maxYOut && result > Mathf.Abs(maxYOut))
                        maxYOut = result;
                }
                else if (result < 0)
                {
                    if (Mathf.Abs(result) > Mathf.Abs(maxYOut))
                        maxYOut = result;
                }
            }

            //we are out of range on the X - need to move on the X to complete rotate
            if (maxYOut != 0)
            {
                MovePiece(MPosX - m_startDragOffsetXUnits , MPosY - m_startDragOffsetYUnits + maxYOut);
            }
            
        }
        
        for (int i = 0; i < squeres.Length; i++)
        {
            MSqueres[i].MoveMe(squeres[i]);
        }

        CalculateCenter();
        
    }

    private void MovePiece(int posX, int posY)
    {
        //need to check if this move is valid or not
        Vector2 tempPos = new Vector2(posX - m_startDragOffsetXUnits, posY - m_startDragOffsetYUnits);
        bool moveValid = true;

        foreach (SquerePieceView item in MSqueres)
        {
            if (Utils.IsInRange(item.GetMyOffset() + tempPos) == false)
                moveValid = false;
        }

        if (moveValid==false)
            return;

        m_posX = posX - m_startDragOffsetXUnits;
        m_posY = posY - m_startDragOffsetYUnits;
        
        MoveToBoardPosition(null,m_posX,m_posY,0.1f);
        
        foreach (SquerePieceView item in MSqueres)
            item.UpdateMyPositionDisplay();

    }

    public void MyInventoryPieceClicked()
    {
        if(m_inInventory)
            DragStarter(new Vector2());
    }
    
    
    public void DragStarter(Vector2 vector2)
    {
        if (m_pieceSetForever)
            return;
        
        Debug.Log("PieceClicked: " + vector2);

        if (m_inInventory)
        {
            ManagerView.Instace.InventoryPieceClicked(this.m_pieceModel);
            m_inInventory = false;
            return;
        }

        PiecePlacedRemovedFromBoard(false);
        
        m_startDragOffsetXUnits = Convert.ToInt32(vector2.x);
        m_startDragOffsetYUnits =  Convert.ToInt32(vector2.y);
        
        m_drag = true;
        ManagerView.Instace.DraggingStarted(this,m_piecePlaced);
        m_piecePlaced = false;
        
    }

    public void DragEnded()
    {
        if (m_pieceSetForever)
            return;
        
        m_startDragOffsetXUnits = 0;
        m_startDragOffsetYUnits = 0;

        m_drag = false;
        ManagerView.Instace.DraggingEnded();
    }

    public Vector2 GetMyPos()
    {
        return new Vector2(MPosX, MPosY);
    }

    public void MoveToBoardPosition(Action moveDone,int posX,int posY,float moveTime)
    {
        m_posX = posX;
        m_posY = posY;
        m_moveDone = moveDone;
        m_display.text = MPosX + "," + MPosY;
        
        Vector2 vector2 = new Vector2(ManagerView.PIECE_WIDTH * (posX-1) , ManagerView.PIECE_WIDTH * (posY-1));

        ManagerView.Instace.PieceWasMoved(vector2);
        
        vector2 -= ManagerView.Instace.MInventoryPostion;
        vector2 += ManagerView.Instace.MBoardPosition;
        
        MoveAndScaleTween(vector2, ManagerView.GAME_SCALE,  moveTime, moveDone);
    }

    public Vector2 GetControllerPosition()
    {
        return  new Vector2(ManagerView.PIECE_WIDTH * (m_posX-1) , ManagerView.PIECE_WIDTH * (m_posY-1));
    }
 
    public void MoveToInventory(Action moveDone, bool snap)
    {
        
        m_moveDone = moveDone;
        m_inInventory = true;

        float moveTime = MOVE_TIME;

        if (snap)
            moveTime = 0.01f;

        
        MoveAndScaleTween(m_inventroyPosition + m_prefabCenterPixelOffset, ManagerView.INVENTORY_SCALE, moveTime,moveDone);
    }
    
    public void MoveAndScaleTween(Vector2 targetPostion, float targetScale,float moveTime,Action moveDone)
    {
        m_moveDone = moveDone;

        if (moveTime == 0)
        {
            if (m_is3D)
            {
                transform.localPosition = targetPostion;
                transform.localScale = new Vector3(targetScale,targetScale,targetScale);
            }
            else
            {
                m_rect.localPosition = targetPostion;
                m_rect.localScale = new Vector3(targetScale,targetScale,targetScale);
            }
         
            m_moveDone?.Invoke();
        }
        else
        {
            if (m_is3D)
            {
                Sequence sequence = DOTween.Sequence();
                sequence.Insert(0,transform.DOScale(new Vector3(targetScale, targetScale, targetScale),moveTime));
                sequence.Insert(0,transform.DOLocalMove((Vector3)targetPostion,moveTime));
                sequence.OnComplete(() =>
                {
                    m_moveDone?.Invoke();
                });
                sequence.SetEase(Ease.Linear);
                sequence.Play();
            }
            else
            {
                Sequence sequence = DOTween.Sequence();
                sequence.Insert(0,m_rect.DOScale(new Vector3(targetScale, targetScale, targetScale),moveTime));
                sequence.Insert(0,m_rect.DOLocalMove((Vector3)targetPostion,moveTime));
                sequence.OnComplete(() =>
                {
                    m_moveDone?.Invoke();
                });
                sequence.SetEase(Ease.Linear);
                sequence.Play();    
            }
            
        }
        
     
    }

    private void MoveScaleDone()
    {
        m_moveDone?.Invoke();
    }
}
