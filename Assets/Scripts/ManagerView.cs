﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ManagerView : MonoBehaviour
{
    #region varibles
    
    public const float INVENTORY_SCALE = 0.25f;
    public const float GAME_SCALE = 1f;
    public const float PIECE_WIDTH = 140f;

    public const int WIDTH = 6;
    public const int HEIGHT = 10;

    const int NUM_PIECES = 12;

    public static ManagerView Instace;

    [SerializeField] private Color STATIC_PIECE_VIEW_COLOR = Color.white;
    [SerializeField] private List<Color> m_piecesColors = new List<Color>();   
    
    [SerializeField] GameObject m_boardCubePrefab;
    [SerializeField] GameObject m_singleSquerePrefab;
    [SerializeField] GameObject m_pieceHolder;
    [SerializeField] PieceCcontrolView m_pieceController;
    [SerializeField] GameObject m_inventoryCubePrefab;

    [SerializeField] Transform m_boardHolder;
    [SerializeField] Transform m_boardBlocksHolder;
    [SerializeField] Transform m_piecesHolder;
    [SerializeField] Transform m_inventoryHolder;

    [SerializeField] RectTransform canvas;

    [SerializeField] private GameObject m_gameScene;
    [SerializeField] private GameObject m_splashScene;
    [SerializeField] private GameObject m_menuScene;
    [SerializeField] private TMP_InputField m_inputField;
    
    private Image[,] m_boardVisual = new Image[WIDTH, HEIGHT];
    private List<PieceView> m_levelPieces = new List<PieceView>();
    float m_widthPixel;
    float m_heightPixel;
    float m_canvasScale;
    float m_screenWidth;
    float m_screenHeight;

    Vector2 m_screenToPixels;
    private Vector2 m_inventoryPostion;
    private Vector2 m_boardPosition;

    private int m_boardMoveX = 0;
    private int m_boardMoveY = -200;
    
    float m_startPointX ;
    float m_startPointY ;

    private PieceView m_currPieceView;
    public Vector3 eulerAngles;

    public Vector2 ScreenToPixels { get => m_screenToPixels; set => m_screenToPixels = value; }
    public GameObject SingleSquerePrefab { get => m_singleSquerePrefab; set => m_singleSquerePrefab = value; }

    public Transform MBoardHolder => m_boardHolder;

    public Vector2 MInventoryPostion => m_inventoryPostion;

    public Vector2 MBoardPosition => m_boardPosition;

#endregion
    private void Awake()
    {
        Instace = this;
    }

    private void Start()
    {
        BuildScreenParams();
        ModelManager.Instance.InitData();
        m_menuScene.GetComponent<MenuView>().FillSOlutionSlider(ModelManager.Instance.MSolutions.Count-1);
        
        m_gameScene.SetActive(false);
        m_splashScene.SetActive(true);
    }

    private void BuildScreenParams()
    {
        m_widthPixel = canvas.rect.width;
        m_heightPixel = canvas.rect.height;
        m_canvasScale = canvas.localScale.x;
        m_screenWidth = Screen.width;
        m_screenHeight = Screen.height;

        m_screenToPixels = new Vector2(m_widthPixel / m_screenWidth, m_heightPixel / m_screenHeight);


        Debug.Log("Width Pixel: " + m_widthPixel);
        Debug.Log("Height Pixel: " + m_heightPixel);
        Debug.Log("Screen Width: " + m_screenWidth);
        Debug.Log("Screen Height: " + m_screenHeight);

        Debug.Log("Aspect: " + Camera.main.aspect);

        Debug.Log("Screen: " + m_screenHeight / m_screenWidth);
    }
    
    public void BuildAllPieces(Level level)
    {
        //get level data from Model
        
        //generate all the pieces in their rotation or random based on level data

        m_levelPieces.Clear();
        int counter = 0;
        
        foreach (Piece levelPiece in level.LevelPieces)
        {
        
            float totaWidth = m_inventoryHolder.gameObject.GetComponent<RectTransform>().rect.width;

            float pieceSpace = totaWidth / 6f;

            float startXPos = pieceSpace / 2f;
            
            int posY = 50;
            if (counter > 5)
                posY = -150;

            Vector2 position = new Vector2(-450 + counter % 6 * 180, posY);

            GameObject inventoryPiece = Instantiate(m_inventoryCubePrefab, m_inventoryHolder);
            
            GameObject inventoryPieceObject =  GeneratePiece(levelPiece,m_inventoryHolder,position ,levelPiece.PositionEnabled );

            PieceView pieceView = inventoryPieceObject.GetComponent<PieceView>();
            
            m_levelPieces.Add(pieceView);
            
            inventoryPiece.GetComponent<InveotoryPieceView>().init(pieceView,position);
            
            counter++;
        }

    }
    
    
    public void BuildBoardVisual()
    {
        for (int i = 0; i < WIDTH; i++)
        {
            for (int j = 0; j < HEIGHT; j++)
            {
                GameObject temp = Instantiate(m_boardCubePrefab, m_boardBlocksHolder);
                temp.transform.localPosition = new Vector3(i * PIECE_WIDTH, j * PIECE_WIDTH, 0);
                temp.GetComponent<RectTransform>().sizeDelta = new Vector3(PIECE_WIDTH, PIECE_WIDTH);
                temp.GetComponentInChildren<TMP_Text>().text = (i + 1) + "," + (j + 1);
                
                temp.GetComponentInChildren<TMP_Text>().text = "";
                
                m_boardVisual[i, j] = temp.GetComponent<Image>();
            }
        }

        MBoardHolder.localPosition = new Vector2(-WIDTH / 2 * PIECE_WIDTH + PIECE_WIDTH/2 + m_boardMoveX,
            -HEIGHT / 2 * PIECE_WIDTH + PIECE_WIDTH/2+m_boardMoveY);
        m_piecesHolder.SetParent(m_boardHolder);
        m_piecesHolder.localPosition = new Vector3();
        
        m_startPointX = m_widthPixel/2f - WIDTH / 2f * PIECE_WIDTH + m_boardMoveX;
        m_startPointY = m_heightPixel/2f - HEIGHT / 2f * PIECE_WIDTH + m_boardMoveY;

        m_boardPosition = MBoardHolder.localPosition;
        m_inventoryPostion = m_inventoryHolder.GetComponent<RectTransform>().localPosition;

    }

    public void InventoryPieceClicked(Piece piece)
    {
        //if you have an active piece in your hand
        if (m_currPieceView != null)
        {
            if(CanPutPiece())
                PutRemovePieceOnBoard(true);
            else
                PutPieceBackToInventory(m_currPieceView);
        }
          
        m_currPieceView = GetPieceViewFromModel(piece);
        m_currPieceView.transform.SetAsLastSibling();

        
        m_currPieceView.MoveToBoardPosition(null,4,6,PieceView.MOVE_TIME);
        m_pieceController.Init(piece,m_currPieceView,CanPutPiece());
        
    }

    
    private PieceView GetPieceViewFromModel(Piece piece)
    {
        foreach (PieceView levelPiece in m_levelPieces)
        {
            if (levelPiece.GetComponent<PieceView>().MPieceModel == piece)
                return levelPiece;
        }

        return null;
    }
    
    public GameObject GeneratePiece(Piece pieceModel,Transform parent, Vector2 inventoryPosition,bool inInventory)
    {
     
        GameObject currPiece = Instantiate(m_pieceHolder);
        currPiece.transform.SetParent(parent);

        float scale = GAME_SCALE;

        if (inInventory)
            scale = INVENTORY_SCALE;
        
        Color color = m_piecesColors[pieceModel.MIndex-1];

        PieceView pieceView = currPiece.GetComponent<PieceView>();
        
        pieceView.Init(pieceModel,scale,inInventory,pieceModel.MPosX,pieceModel.MPosY,
            pieceModel.Offsets,color,inventoryPosition);

        return currPiece;
    }

    private bool CanPutPiece()
    {
        bool allEmpty = true;
        int currPieceX = m_currPieceView.MPosX;
        int currPieceY = m_currPieceView.MPosY;
        
        foreach (SquerePieceView squere in m_currPieceView.MSqueres)
            if (ModelManager.Instance.IsBoardFull(currPieceX + squere.GetPosX(), currPieceY + squere.GetPosY()))
                allEmpty = false;
        
        return allEmpty;
    }

    private void PutRemovePieceOnBoard(bool put)
    {
        int currPieceX = m_currPieceView.MPosX;
        int currPieceY = m_currPieceView.MPosY;
        
        m_currPieceView.PiecePlacedRemovedFromBoard(put);
       

        MarkBoardWithPiece(currPieceX, currPieceY, put,m_currPieceView.MSqueres);

    }

    public void MarkBoardWithPiece(int currPieceX,int currPieceY,bool put, List<SquerePieceView> squeres)
    {
        foreach (SquerePieceView squere in squeres)
        {
            int posX = currPieceX + squere.GetPosX();
            int posY = currPieceY + squere.GetPosY();
            ModelManager.Instance.SetSquere(posX,posY,put);
          
            if(put)
                m_boardVisual[posX - 1, posY - 1].gameObject.GetComponentInChildren<TMP_Text>().text = "X";
            else 
                m_boardVisual[posX - 1, posY - 1].gameObject.GetComponentInChildren<TMP_Text>().text = "";

        }
    }
    
    public void DraggingStarted(PieceView pieceView,bool piecePositioned)
    {
        m_currPieceView = pieceView;
        m_currPieceView.transform.SetAsLastSibling();
        m_pieceController.Init(pieceView.MPieceModel,m_currPieceView,CanPutPiece());
        
        //flag to indicate if the piece was already positioned - so we can clear the board
        if(piecePositioned)
            PutRemovePieceOnBoard(false);
        
        m_pieceController.ShowHideUI(false,CanPutPiece());
    }

    public void DraggingEnded()
    {
        m_pieceController.ShowHideUI(true,CanPutPiece());
    }
    
    public void PieceWasMoved(Vector2 vector2)
    {
        m_pieceController.Move(vector2);
    }

    public Vector2[] GetRotatedVectors(Vector2[] origVerts, bool cw, bool mirror,Piece piece)
    {
        Vector2[] newVerts = new Vector2[origVerts.Length];
        
        int rotationAount = cw?90:-90;
        Quaternion rotation;
        
        if (!mirror)
            piece.CurrRotation += rotationAount%360;
        else
            piece.CurrMirror += 180 % 360;
       
        rotation = Quaternion.Euler(eulerAngles.x,piece.CurrMirror , piece.CurrRotation);
        Matrix4x4 m = Matrix4x4.Rotate(rotation);

        int i = 0;
        while (i < origVerts.Length)
        {
            newVerts[i] = m.MultiplyPoint3x4(origVerts[i]);
            i++;
        }

        return newVerts;
    }
   
    public Vector2[] GetRotatedVectors(Vector2[] origVerts, int rotationAngle, int mirror)
    {
        Vector2[] newVerts = new Vector2[origVerts.Length];
        
        Quaternion rotation;
       
        rotation = Quaternion.Euler(eulerAngles.x,mirror , rotationAngle);
        Matrix4x4 m = Matrix4x4.Rotate(rotation);

        int i = 0;
        while (i < origVerts.Length)
        {
            newVerts[i] = m.MultiplyPoint3x4(origVerts[i]);
            i++;
        }

        return newVerts;
    }
    
    public Vector2 PointToSquere(Vector2 point)
    {
        int x = Mathf.CeilToInt((point.x - m_startPointX) / PIECE_WIDTH);
        int y = Mathf.CeilToInt((point.y - m_startPointY) / PIECE_WIDTH);

        x = Mathf.Min(WIDTH, x);
        y = Mathf.Min(HEIGHT, y);
        
        return new Vector2(x,y);
    }

    public Vector2 SquerePosToSquereIndex(Vector2 point)
    {
        float x = point.x - MBoardHolder.transform.localPosition.x + m_boardMoveX*2;
        float y = point.y - MBoardHolder.transform.localPosition.y + m_boardMoveY*2;

        //convert to full numbers
        x = Convert.ToInt32(x) / PIECE_WIDTH + 1;
        y = Convert.ToInt32(y) / PIECE_WIDTH + 1;

        x = Mathf.Min(WIDTH, x);
        y = Mathf.Min(HEIGHT, y);

        return new Vector2(x, y);
    }


    public void ConfirmClicked()
    {
        PutRemovePieceOnBoard(true);
        m_pieceController.ShowHideUI(false,false);
        m_currPieceView = null;
    }

    public void RotateClicked(bool cw)
    {
        Vector2[] newVerts = GetRotatedVectors(m_currPieceView.MPieceModel.Offsets, cw, false,m_currPieceView.MPieceModel);
        m_currPieceView.RotatePiece(newVerts);
        m_pieceController.ShowHideConfirm(CanPutPiece());
        m_pieceController.UpdateState(m_currPieceView.MPieceModel);
        
    }

    public void MirrorClicked()
    {
        Vector2[] newVerts = GetRotatedVectors(m_currPieceView.MPieceModel.Offsets, true, true,m_currPieceView.MPieceModel);
        m_currPieceView.RotatePiece(newVerts);
        m_pieceController.ShowHideConfirm(CanPutPiece());
        m_pieceController.UpdateState(m_currPieceView.MPieceModel);
        
    }

    public void TrashClicked()
    {
        PutPieceBackToInventory(m_currPieceView);
        m_currPieceView = null;
    }

    private void PutPieceBackToInventory(PieceView pieceView)
    {
        pieceView.MoveToInventory(null,false);
        m_pieceController.ShowHideUI(false,false);
    }

    private void DestoryBoard()
    {
        foreach (Transform item in m_boardBlocksHolder)
            Destroy(item.gameObject);

        foreach (Transform item in m_inventoryHolder)
            Destroy(item.gameObject);

        
        m_pieceController.Reset();
    }
    
    public void BackToMenuClicked()
    {
        m_menuScene.SetActive(true);
        m_gameScene.SetActive(false);
        DestoryBoard();
    }

    public void SpalshClicked()
    {
        m_splashScene.SetActive(false);
        m_menuScene.SetActive(true);
    }

    public void MenuClicked(int diff,int solutionIndex = -1)
    {
        m_menuScene.SetActive(false);
        m_gameScene.SetActive(true);
        
        Debug.Log("Diff: "+ diff  + " SIndex: " +solutionIndex);
        
        GameController.Instance.Init(diff,solutionIndex);
      
    }

    public void SnapClicked()
    {
        if (m_inputField.gameObject.activeInHierarchy)
            Debug.Log(m_inputField.text);

        string data = "";

        for (int i = 0; i < m_levelPieces.Count; i++)
        {
            PieceView currPieceView = m_levelPieces[i];
            data += currPieceView.MPosX + "," + currPieceView.MPosY + "," + currPieceView.MPieceModel.CurrRotation +
                    "," + currPieceView.MPieceModel.CurrMirror + ";";
        }

        data += "$";
        m_inputField.text = data; 
        
        m_inputField.gameObject.SetActive(!m_inputField.gameObject.activeInHierarchy);
    }
    
}
