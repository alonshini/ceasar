﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InveotoryPieceView : MonoBehaviour, IBeginDragHandler,IDragHandler
{

    public bool m_is3D;
    private PieceView m_pieceView;
    
    public void init(PieceView pieceView, Vector2 position)
    {

        m_pieceView = pieceView;
        if (m_is3D)
        {
            transform.localPosition = position;
        }
        else
        {
            GetComponent<RectTransform>().localPosition = position;
        }
    }

    public void ButtonClicked()
    {
        m_pieceView.MyInventoryPieceClicked();
    }

  
    public void OnBeginDrag(PointerEventData eventData)
    {
        m_pieceView.MyInventoryPieceClicked();
    }

    public void OnDrag(PointerEventData eventData)
    {
        m_pieceView.MyInventoryPieceClicked();
    }
}
