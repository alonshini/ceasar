﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelManager : MonoBehaviour
{
    
    Board m_board;
    List<Piece> m_pieces = new List<Piece>();
    private List<Solution> m_solutions = new List<Solution>();
    private List<Level> m_levels = new List<Level>();

    public static ModelManager Instance;

    public List<Piece> Pieces { get => m_pieces;}

    public List<Solution> MSolutions => m_solutions;

    private void Awake()
    {
        Instance = this;
    }

    public void InitData()
    {
        m_levels.Clear();
        m_pieces.Clear();
        CreatePiecesModel(ManagerView.WIDTH, ManagerView.HEIGHT);
        BuildSolutionData();
        m_levels.Add(new Level(m_pieces,MSolutions[0]));
    }

    public void CreateNewBoard()
    {
        //build the board
        m_board = new Board(ManagerView.WIDTH,ManagerView.HEIGHT);
    }
    
    
    public void BuildSolutionData()
    {
        string data = Utils.ReadDataFile();
        
        string[] solutions = data.Split('$');

        foreach (string singleSolution in solutions)
        {
            if (singleSolution != "")
            {
                List<Piece> pieceSolutions = new List<Piece>();
                string[] pieces = singleSolution.Split(';');
                int pieceIndex = 1;
                foreach (string piece in pieces)
                {
                    if (piece != "")
                    {
                        string[] pieceParts = piece.Split(',');
                        Piece pieceSolution = new Piece( pieceIndex,Convert.ToInt32(pieceParts[0]),
                            Convert.ToInt32(pieceParts[1]), Convert.ToInt32(pieceParts[2]), Convert.ToInt32(pieceParts[3]));
                        pieceSolutions.Add(pieceSolution);
                        pieceIndex++;
                    }
                
                }
                Solution solution = new Solution(pieceSolutions);
                m_solutions.Add(solution);
            }
        }
    }
    
    
    private void CreatePiecesModel(int width, int height)
    {
        //build pieces
        // 1 - XXCXX
        Vector2[] m_piece = new Vector2[5];
        m_piece[0] = new Vector2(-2, 0);
        m_piece[1] =new Vector2(-1, 0);
        m_piece[2] =new Vector2(0, 0);
        m_piece[3] =new Vector2(1, 0);
        m_piece[4] =new Vector2(2, 0);

        m_pieces.Add(new Piece(1,m_piece,false,true));

        // 2 - X X
        //     XCX

        m_piece = new Vector2[5];
        m_piece[0] =new Vector2(-1, 1);
        m_piece[1] =new Vector2(-1, 0);
        m_piece[2] =new Vector2(0, 0);
        m_piece[3] =new Vector2(1, 1);
        m_piece[4] =new Vector2(1, 0);

        m_pieces.Add(new Piece(2,m_piece,false,true));

        //       XX
        // 3 - XXC

        m_piece = new Vector2[5];
        m_piece[0] =new Vector2(-2, 0);
        m_piece[1] =new Vector2(-1, 0);
        m_piece[2] =new Vector2(0, 0);
        m_piece[3] =new Vector2(0, 1);
        m_piece[4] =new Vector2(1, 1);

        m_pieces.Add(new Piece(3,m_piece,true,true));

        //       X
        // 4 - XXCX

        m_piece = new Vector2[5];
        m_piece[0] =new Vector2(-1, 0);
        m_piece[1] =new Vector2(-2, 0);
        m_piece[2] =new Vector2(0, 0);
        m_piece[3] =new Vector2(1, 0);
        m_piece[4] =new Vector2(0, 1);

        m_pieces.Add(new Piece(4,m_piece,true,true));

        //        X
        // 5 - XXCX

        m_piece = new Vector2[5];
        m_piece[0] =new Vector2(-1, 0);
        m_piece[1] =new Vector2(-2, 0);
        m_piece[2] =new Vector2(0, 0);
        m_piece[3] =new Vector2(1, 0);
        m_piece[4] =new Vector2(1, 1);

        m_pieces.Add(new Piece(5,m_piece,true,true));

        // 6 -  X
        //     XCX
        //      X

        m_piece = new Vector2[5];
        m_piece[0] =new Vector2(-1, 0);
        m_piece[1] =new Vector2(0, 1);
        m_piece[2] =new Vector2(0, 0);
        m_piece[3] =new Vector2(1, 0);
        m_piece[4] =new Vector2(0, -1);

        m_pieces.Add(new Piece(6,m_piece,false,false));

        // 7 - X
        //     XC
        //      XX

        m_piece = new Vector2[5];
        m_piece[0] =new Vector2(-1, 1);
        m_piece[1] =new Vector2(-1, 0);
        m_piece[2] =new Vector2(0, 0);
        m_piece[3] =new Vector2(0, -1);
        m_piece[4] =new Vector2(1, -1);

        m_pieces.Add(new Piece(7,m_piece,false,true));

        // 8 -  XX
        //      C
        //     XX

        m_piece = new Vector2[5];
        m_piece[0] =new Vector2(-1, -1);
        m_piece[1] =new Vector2(0, -1);
        m_piece[2] =new Vector2(0, 0);
        m_piece[3] =new Vector2(0, 1);
        m_piece[4] =new Vector2(1, 1);

        m_pieces.Add(new Piece(8,m_piece,true,true));

        // 9 -  X
        //      X
        //      CXX

        m_piece = new Vector2[5];
        m_piece[0] =new Vector2(0, 2);
        m_piece[1] =new Vector2(0, 1);
        m_piece[2] =new Vector2(0, 0);
        m_piece[3] =new Vector2(1, 0);
        m_piece[4] =new Vector2(2, 0);

        m_pieces.Add(new Piece(9,m_piece,false,true));

        // 10 -   X
        //      XXC
        //        X

        m_piece = new Vector2[5];
        m_piece[0] =new Vector2(-2, 0);
        m_piece[1] =new Vector2(-1, 0);
        m_piece[2] =new Vector2(0, 0);
        m_piece[3] =new Vector2(0, 1);
        m_piece[4] =new Vector2(0, -1);

        m_pieces.Add(new Piece(10,m_piece,false,true));

        // 11 - XXC
        //      XX
        //        

        m_piece = new Vector2[5];
        m_piece[0] =new Vector2(-1, 0);
        m_piece[1] =new Vector2(-1, -1);
        m_piece[2] =new Vector2(0, 0);
        m_piece[3] =new Vector2(0, -1);
        m_piece[4] =new Vector2(1, 0);

        m_pieces.Add(new Piece(11,m_piece,true,true));

        // 12 - XX
        //       CX
        //       X 

        m_piece = new Vector2[5];
        m_piece[0] =new Vector2(-1, 1);
        m_piece[1] =new Vector2(0, 1);
        m_piece[2] =new Vector2(0, 0);
        m_piece[3] =new Vector2(1, 0);
        m_piece[4] =new Vector2(0, -1);

        m_pieces.Add(new Piece(12,m_piece,true,true));
       
    }

    public List<Piece> GetNewLevelPieces()
    {
        List<Piece> newPieces = new List<Piece>();

        foreach (Piece piece in m_pieces)
        {
            newPieces.Add(new Piece(piece));
        }
        
        return newPieces;
    }
    
    
    public bool IsBoardFull(int posX, int posy)
    {
        return m_board.IsFull(posX-1, posy-1);
    }
    
    public void SetSquere(int posX, int posy,bool full)
    {
        m_board.SetSquere(posX-1, posy-1,full);
    }

    
    
}
