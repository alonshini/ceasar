﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piece
{
    private int m_index;
    Vector2[] m_offsets = new Vector2[5];
    bool m_mirrorEnabled = false;
    private bool m_rotationEnabled = false;
    private bool m_positionEnabled = true;
    
    int m_currRotation = 0;
    int m_currMirror = 0;
    private int m_posX=4;
    private int m_posY=4;

    public Piece(Piece piece)
    {
        Offsets = piece.Offsets;
        MirrorEnabled = piece.MirrorEnabled;
        RotationEnabled = piece.RotationEnabled;
        m_index = piece.MIndex;
        m_currMirror = piece.m_currMirror;
        m_currRotation = piece.m_currRotation;
        m_posX = piece.MPosX;
        m_posY = piece.MPosY;
        
    }

    public Piece(int index, int posx, int posy, int rotation, int mirror)
    {
        m_index = index;
        m_posX = posx;
        m_posY = posy;
        m_currRotation = rotation;
        m_currMirror = mirror;
    }

    public Piece(int mIndex, Vector2[] offsets, bool MirrorEnabled, bool rotationEnabled)
    {
        m_index = mIndex;
        Offsets = offsets;
        m_mirrorEnabled = MirrorEnabled;
        m_rotationEnabled = rotationEnabled;
        
    }

    public void SetPieceAdditionalParams(int posx, int posy, int currRotation, int currMirror)
    {
        m_posX = posx;
        m_posY = posy;
        m_currRotation = currRotation;
        m_currMirror = currMirror;
    }

    public void SetPositionData(int posX, int posY)
    {
        m_posX = posX;
        m_posY = posY;
    }

    public int MIndex => m_index;

    public int CurrRotation
    {
        get => m_currRotation;
        set => m_currRotation = value;
    }

    public int CurrMirror
    {
        get => m_currMirror;
        set => m_currMirror = value;
    }

    public Vector2[] Offsets
    {
        get => m_offsets;
        set => m_offsets = value;
    }

    public bool RotationEnabled
    {
        get => m_rotationEnabled;
        set => m_rotationEnabled = value;
    }

    public bool MirrorEnabled
    {
        get => m_mirrorEnabled;
        set => m_mirrorEnabled = value;
    }

    public bool PositionEnabled
    {
        get => m_positionEnabled;
        set => m_positionEnabled = value;
    }

    public int MPosX => m_posX;

    public int MPosY => m_posY;
}
