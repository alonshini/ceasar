﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class GameController : MonoBehaviour
{

    public const int NUM_SHAPES = 12;
    public static GameController Instance;

    int m_positionsLocked = 0;
    int m_rotateMirrorLocked = 0;
    int m_free = 0;
    
    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
    }

    //init is called after the model is ready
    public void Init(int diff,int sliderSolutionIndex)
    {
        int solutionIndex = sliderSolutionIndex;

        if (solutionIndex == -1)
            solutionIndex = UnityEngine.Random.Range(0, ModelManager.Instance.MSolutions.Count);
        
        ManagerView.Instace.BuildBoardVisual();
        ModelManager.Instance.CreateNewBoard();
        
        Level newLevel = new Level(ModelManager.Instance.GetNewLevelPieces(), ModelManager.Instance.MSolutions[solutionIndex]);
        
        StartGame(newLevel,diff);
    }

    //level already comes with a specific configuration and difficulty
    public void StartGame(Level level, int diff)
    {
        GenerateDiffParams(diff);

        if (diff == 0)
        {
            m_positionsLocked = 0;
            m_rotateMirrorLocked = 0;
        }
        else if (diff == 1000)
        {
            m_positionsLocked = 12;
            m_rotateMirrorLocked = 0;
        }
        
        
        GenerateLevelByDiff(level,m_positionsLocked,m_rotateMirrorLocked);

        ManagerView.Instace.BuildAllPieces(level);
    
    }

    
    
    private void GenerateDiffParams(int diff)
    {
        if (diff <= 100)
        {
            m_free = Convert.ToInt32(diff / 10);
            m_positionsLocked = Convert.ToInt32(Utils.remap(diff, 1, 100, 10-m_free, 8-m_free));
            m_rotateMirrorLocked = 12 - m_free - m_positionsLocked;
            
            Debug.Log("Diff= " + diff + " Free= " + m_free + " positionLocked= " + m_positionsLocked + " rotateLocked= " + m_rotateMirrorLocked);
        }
        
    }
    
    private void GenerateLevelByDiff(Level level, int positionLocked, int rotateMirrorLocked)
    {
        bool done = false;
        bool pieceDataSet = false;
        int counter = 0; 
        
        int currPieceIndex=0;
        List<int> pieces = new List<int>();

        for (int i = 0; i < NUM_SHAPES; i++)
            pieces.Add(i);
        
        pieces.Shuffle();

        for (int i = 0; i < positionLocked; i++)
        {
            currPieceIndex = pieces[0];
            Piece pieceSolution= level.LevelSolution.LevelSolution.Find(x => x.MIndex == currPieceIndex+1);

            Piece piece = level.LevelPieces.Find(x => x.MIndex == currPieceIndex+1);
            pieces.RemoveAt(0);

            //put it already in its position
            piece.PositionEnabled = false;
            piece.SetPositionData(pieceSolution.MPosX,pieceSolution.MPosY);
            piece.RotationEnabled = false;
            piece.MirrorEnabled = false;
            piece.CurrRotation = pieceSolution.CurrRotation;
            piece.CurrMirror = pieceSolution.CurrMirror;

            level.SetLevelData(currPieceIndex, piece);
        }

        for (int i = 0; i < rotateMirrorLocked; i++)
        {
            currPieceIndex = pieces[0];
            Piece pieceSolution= level.LevelSolution.LevelSolution.Find(x => x.MIndex == currPieceIndex+1);
            Piece piece = level.LevelPieces.Find(x => x.MIndex == currPieceIndex+1);

            bool disableRotate = false;

            if (!piece.MirrorEnabled)
                disableRotate = true;
            else
            {
                //maybe mirror - maybe rotate
                disableRotate = UnityEngine.Random.value > 0.5f;
            }
            
            if (disableRotate)
            {
                piece.RotationEnabled = false;
                //set the piece to its needed rotation
                piece.CurrRotation = pieceSolution.CurrRotation;
                
                if(!piece.MirrorEnabled)
                    piece.CurrMirror = pieceSolution.CurrMirror;
            }
            else
            {
                //disable mirror
                piece.MirrorEnabled = false;
                //set the piece to its needed rotation
                piece.CurrMirror = pieceSolution.CurrMirror;

                if(!piece.RotationEnabled)
                    piece.CurrRotation = pieceSolution.CurrRotation;
                else
                    piece.CurrRotation = UnityEngine.Random.Range(0, 4) * 90;

            }
            
            level.SetLevelData(currPieceIndex, piece);
        }

    }
   

}
