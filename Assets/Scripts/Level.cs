﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level
{
    readonly List<Piece> m_levelPieces = new List<Piece>();
    private Solution m_solution;


    
    public Level(List<Piece> levelPieces,Solution solution)
    {
        m_levelPieces = levelPieces;
        m_solution = solution;
    }

    public void SetLevelData(int index, Piece piece)
    {
        m_levelPieces[index] = piece;
    }
    
    public List<Piece> LevelPieces => m_levelPieces;

    public Solution LevelSolution => m_solution;
}
