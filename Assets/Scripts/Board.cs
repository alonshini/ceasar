﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board 
{
    int[,] m_board;

    public Board(int width, int height)
    {
        m_board = new int[width, height];

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                m_board[i, j] = 0;
            }
        }
    }

    public void SetSquere(int xpos, int ypos,bool full)
    {
        m_board[xpos, ypos] = full == true ? 1 : 0;
    }

    public bool IsFull(int xpos, int ypos)
    {
        return m_board[xpos, ypos] == 1;
    }

}
