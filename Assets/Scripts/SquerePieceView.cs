﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SquerePieceView : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    
    Vector2 m_myOffset = new Vector2();
    Action<Vector2> m_clickCallback;
    Action m_dragEnded;
    PieceView m_parent;
    private RectTransform m_rect;
    [SerializeField] private GameObject m_lockImage;
    [SerializeField] private TMP_Text m_displayText;
    private bool m_displayOnly;
    private Color m_darkenColor;
    private Color m_color;

    

    public void Init(Vector2 vector2, Action<Vector2> ClickedCallback, Action DragEnded,Color color, PieceView parent, bool displayOnly)
    {
        m_myOffset = vector2;
        m_clickCallback = ClickedCallback;
        m_dragEnded = DragEnded;
        m_parent = parent;
        m_displayOnly = displayOnly;
        m_rect = GetComponent<RectTransform>();
        m_rect.localScale = new Vector3(1f, 1f, 1f);
        m_rect.localPosition = new Vector3(vector2.x * ManagerView.PIECE_WIDTH, vector2.y * ManagerView.PIECE_WIDTH, 0);
        m_rect.sizeDelta = new Vector3( ManagerView.PIECE_WIDTH, ManagerView.PIECE_WIDTH);
        m_color = color;
        m_darkenColor = Utils.GenerateDarkenColorVersion(m_color,0.5f);
        //m_darkenColor = new Color(m_darkenColor.r, m_darkenColor.g, m_darkenColor.b, 0.25f);
        GetComponent<Image>().color = m_color;
        ;
        if (displayOnly)
            GetComponent<Image>().raycastTarget = false;
        
        //when using the squeres as display only
        if(!m_displayOnly)
            UpdateMyPositionDisplay();
    }

    public void UpdateMyPositionDisplay()
    {
        Vector2 vector2b = ManagerView.Instace.SquerePosToSquereIndex((Vector2)GetComponent<RectTransform>().localPosition);

        Vector2 vector2 = m_parent.GetMyPos() + m_myOffset;

        int posX = Convert.ToInt32(m_parent.GetMyPos().x);
        posX += Convert.ToInt32(m_myOffset.x);

        int posY = Convert.ToInt32(m_parent.GetMyPos().y);
        posY += Convert.ToInt32(m_myOffset.y);


        m_displayText.text = (posX + "," + posY + "\n" + Convert.ToInt32(m_myOffset.x) + "," + Convert.ToInt32(m_myOffset.y));
    }

    public Vector2 GetMyOffset()
    {
        return m_myOffset;
    }

    public int GetPosX()
    {
        return Convert.ToInt32(m_myOffset.x);
    }
    
    public int GetPosY()
    {
        return Convert.ToInt32(m_myOffset.y);
    }
    
    public void MoveMe(Vector2 vector2)
    {
        m_myOffset = vector2;
        m_rect.localPosition = new Vector3(vector2.x * ManagerView.PIECE_WIDTH, vector2.y * ManagerView.PIECE_WIDTH, 0);
        
        if(!m_displayOnly)
            UpdateMyPositionDisplay();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        
    }

    public void OnPointerDown(PointerEventData eventData)
    {
          m_clickCallback(m_myOffset);
    }

   
    public void OnPointerUp(PointerEventData eventData)
    {
      
        m_dragEnded();
    }

   
    public void SetPieceToBoard(bool lockedForever,bool putOnBoard)
    {
        if(lockedForever)
            m_lockImage.SetActive(true);

        GetComponent<Image>().color = putOnBoard?m_darkenColor:m_color;
    }
    
}
