﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticPieceView : MonoBehaviour
{
    private List<SquerePieceView> m_squeres = new List<SquerePieceView>();
    
    public void Init(Transform parent,float scale,Vector2[] squeres,Color color,Vector2 offset)
    {

        GetComponent<RectTransform>().localPosition = offset;
        GetComponent<RectTransform>().localScale = new Vector3(scale, scale, scale);

        foreach (Vector2 item in squeres)
        {
            GameObject part = Instantiate(ManagerView.Instace.SingleSquerePrefab, transform);

            SquerePieceView squerePieceView = part.GetComponent<SquerePieceView>();

            m_squeres.Add(squerePieceView);
            part.GetComponent<SquerePieceView>().Init(item,null, null, color,null,true);

        }

    }
    
    public void UpdateParams(Vector2[] updatedPositions)
    {
        for (int i = 0; i < updatedPositions.Length; i++)
            m_squeres[i].MoveMe(updatedPositions[i]);
    }
}
